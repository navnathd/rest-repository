<html>
<script src="resources/js/angular.min.js"></script>
<script src="resources/js/jquery.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<link rel="stylesheet" href="resources/css/w3.css">
<link rel="stylesheet" href="resources/css/custom.css">
<title>Product List</title>
<body>
	<div class="overlay"></div>
	<div class="modal">
		<div class="g-recaptcha"
			data-sitekey="6LfrmhoTAAAAAMjhdZfDtS6cWk_iFf9s83fcBXFT"></div>

		<div class="w3-col">
			<button id="validate" class="w3-btn w3-padding w3-green">Validate</button>
		</div>
	</div>

	<div ng-app="customProductList" ng-cloak ng-controller="productCtrl"
		class="w3-card-2 w3-margin">
		<header class="w3-container w3-light-grey w3-padding-hor-16">
			<h3 style="text-align: center">Custom Electronic Product List</h3>
		</header>

		<ul class="w3-ul">
			<li ng-repeat="x in products" class="w3-padding-hor-16">{{x.productName}}<span
				ng-click="removeItem($index,x.id)" data={{x.id}}
				style="cursor: pointer;" class="w3-right w3-margin-right">x</span></li>
		</ul>

		<div class="w3-container w3-light-grey w3-padding-hor-16">
			<div class="w3-row w3-margin-top">
				<div class="w3-col s10">
					<input placeholder="Add items here" ng-model="addMe"
						class="w3-input w3-border w3-padding">
				</div>
				<div class="w3-col s2">
					<button ng-click="addItem()" class="w3-btn w3-padding w3-green">Add</button>
				</div>
			</div>
			<p class="w3-padding-left w3-text-red">{{errortext}}</p>
		</div>
	</div>

	<script>
		var app = angular.module("customProductList", []);
		app.controller("productCtrl", function($scope, $http) {

			//get request for accessing product list from rest api
			$http.get('http://localhost:8080/RestApp/webapi/resource/products')
					.success(function(data) {
						$scope.products = data;
					});

			//adding atem into list
			$scope.addItem = function() {
				$scope.errortext = "";
				if (!$scope.addMe) {
					alert("Please enter product item.");
					return;
				}
				
				var flag=false;
				angular.forEach($scope.products, function(value, key) {
					if($scope.addMe==value.productName)
						{
						flag=true;
						}
					});
				
				if(!flag)
				{
					//posting new added product item to restapi server
					$http.post(
							'http://localhost:8080/RestApp/webapi/resource/addproduct/'
									+ $scope.products.length + '/' + $scope.addMe)
							.success(function(data) {
								$scope.products.push(data);
							});
					$scope.addMe = "";
				}
				else
					{
					   $scope.errortext = "The item is already in your shopping list."; 
					   flag=false;
					}
			}

			//remove list item
			$scope.removeItem = function(index,id) {
				
				  var r = confirm("Want to remove product?");
				    if (r == true) {
				    	$scope.errortext = "";
						$scope.products.splice(index, 1);
						$http({method : "DELETE", url : "http://localhost:8080/RestApp/webapi/resource/removeproduct/"+id});
				    } else {
				        return;
				    }
			}
		});
		
		
		$(function() {
			
			$("html").css("overflow","hidden");
			$("#validate").click(function(){
				var gCaptcha = grecaptcha.getResponse();
				var captchaResponse= JSON.parse(validateCaptcha(gCaptcha));
				if(captchaResponse.success)
				{
					$(".overlay").remove();
					$(".modal").remove();
					$("html").css("overflow","visible");
				}
				else
				{
					 alert("please re-validate captcha.");
				}
				
			});
		});
		
		function validateCaptcha(captcha)
		{
			//AJAX POST
			var response;
			var request = $.ajax({
			  url: "http://localhost:8080/RestApp/webapi/resource/validate/"+captcha,
			  method: "POST",
			  dataType: "json",
			  async: false
			});
			 
			request.done(function( msg ) {
			  console.log(msg);
			  response=JSON.stringify(msg);
			});
			 
			request.fail(function( jqXHR, textStatus ) {
				console.log(textStatus);
			});
			
			return response;
		}
		
	</script>
</body>
</html>
