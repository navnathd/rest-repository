package org.nath.restapp.resource;

/**
 * 
 * @author navnath.damale
 *
 */
public class Product {
	private long id;
	private String productName;

	public Product() {
	}

	public Product(long id,String productName)
	{
		this.id=id;
		this.productName=productName;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
}
