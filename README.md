# AngularJS+JavaScript+User Validation : REST #

This project shows simple implementation for REST Web Services using jersey.
It will demonstrate simple operation like add, delete, show with one of the model object.
Have prior added sample electronic list of item statically in rest api.


## Tools And Technologies ##

* Tomcat 1.7
* JDK 1.7+
* Eclipse Marse 2
* Maven 3.0
* Jersey 2.23.1


## Summary of setting up ##

*  Step 1: download and setup maven. refer https://www.mkyong.com/maven/how-to-install-maven-in-windows/
*  Step 2: deploy RestApp.war file on tomcat.
*  Step 3: Access web application as http://localhost:8080/RestApp/
*  Step 4: Validate google captcha.
*  Step 5: Shows custom electronic product list here. Scroll up to bottom and add product item.