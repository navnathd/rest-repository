package org.nath.restapp.dataresource;

import java.util.HashMap;
import java.util.Map;
import org.nath.restapp.resource.Product;

/**
 * 
 * @author navnath.damale
 *
 */
public class DatabaseResource {
	private static DatabaseResource instance;
	private static Map<Long, Product> productList = new HashMap<>();

	private DatabaseResource() {
	}

	public static DatabaseResource getInstance() {
		if (instance == null) {
			return new DatabaseResource();
		}
		return instance;
	}

	public Map<Long, Product> getProducts() {
		productList.put(1L, new Product(1L, "Water Level Controller using 8051 Microcontroller"));
		productList.put(2L, new Product(2, "Car Parking Guard Circuit Using Infrared Sensor"));
		productList.put(3L, new Product(3, "Bidirectional Visitor Counter using 8051"));
		productList.put(4L, new Product(4, "Temperature Controlled DC Fan using Microcontroller"));
		productList.put(5L, new Product(5, "Line Following Robotic Circuit using ATMega8 Microcontroller"));
		productList.put(6L, new Product(6, "Celsius Scale Thermometer using AT89C51"));
		productList.put(7L, new Product(7, "Auto Night Lamp using High Power LED"));
		productList.put(8L, new Product(8, "Density Based Traffic Signal System using Microcontroller"));
		productList.put(9L, new Product(9, "Automatic Washroom Light Switch"));
		productList.put(10L, new Product(10, "Automatic Door Bell With Object Detection"));
		productList.put(11L, new Product(11, "Ding Dong Sound Generator Circuit"));
		productList.put(12L, new Product(12, "PWM based DC Motor Speed Control using Microcontroller"));
		productList.put(13L, new Product(13, "Automatic Railway Gate Controller with High Speed Alerting System"));
		productList.put(14L, new Product(14, "Police Siren Circuit using NE555 Timer"));

		return productList;
	}

}
