package org.nath.restapp.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.nath.restapp.resource.Product;
import org.nath.restapp.service.ProductService;
import org.nath.restapp.validatecaptcha.ValidateCaptcha;

/**
 * 
 * @author navnath.damale
 * Root resource expose at "resource" path
 *
 */
@Path("resource")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductController {

	ProductService productService=ProductService.getInstance();

	/**
	 * Method handling HTTP GET request.The return object 
	 * will be sent to the client as "application/json" media type
	 * @return product list that will be returned as a "application/json" response.
	 */
	@GET
	@Path("products")
	public List<Product> getProductList() {
		return productService.getProducts();
	}

	/**
	 * Method handling HTTP POST request.
	 * Accept json and create new product 
	 */
	@POST
	@Path("addproduct/{id}/{productName}")
	public Product addProduct(@PathParam("id") long id,@PathParam("productName") String productName)
	{
		return productService.addMessage(new Product(id,productName));
	}
	
	/**
	 * Method handling HTTP DELETE request.
	 * Accept product id and remove it from product list
	 */
	@DELETE
	@Path("removeproduct/{id}")
	public void removeProduct(@PathParam("id") Long id)
	{
		productService.removeProduct(id);
	}
	
	
	/**
	 * Method handling HTTP POST request.
	 * Accept captcha and validate it.
	 */
	@POST
	@Path("validate/{captcha}")
	public String addProduct(@PathParam("captcha") String captcha)
	{
		return ValidateCaptcha.validateUser(captcha);
	}
}
