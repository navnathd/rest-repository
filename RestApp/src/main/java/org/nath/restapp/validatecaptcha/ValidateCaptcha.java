package org.nath.restapp.validatecaptcha;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * 
 * @author navnath.damale
 *
 */
public class ValidateCaptcha {
	
	public static String validateUser(String captchaResponse)
	{
		try {

			URL url = new URL("https://www.google.com/recaptcha/api/siteverify");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			/*conn.setRequestProperty("Content-Type", "application/json");*/
			/*JSONObject parameter = new JSONObject();
			parameter.put("secret", "6LfrmhoTAAAAAOCI4hwdvLQu3M7qdXxrHwS7Ki8Y");
			parameter.put("response", captchaResponse);*/
			
			String parameter="secret=6LfrmhoTAAAAAOCI4hwdvLQu3M7qdXxrHwS7Ki8Y&response="+captchaResponse;
			
			OutputStream os = conn.getOutputStream();
			os.write(parameter.toString().getBytes());
			os.flush();

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			StringBuffer cResponse=new StringBuffer();
			while ((output = br.readLine()) != null) {
				cResponse.append(output);
			}
			conn.disconnect();
			
			return cResponse.toString();

		} catch (MalformedURLException e) {

			e.printStackTrace();

		} catch (IOException e) {

			e.printStackTrace();

		}
		return null;
	}

}
