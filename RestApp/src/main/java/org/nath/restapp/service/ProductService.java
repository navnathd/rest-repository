package org.nath.restapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.nath.restapp.dataresource.DatabaseResource;
import org.nath.restapp.resource.Product;

/**
 * 
 * @author navnath.damale
 *
 */
public class ProductService {
	public static ProductService instance;
	public static Map<Long, Product> productList = (DatabaseResource.getInstance()).getProducts();

	private ProductService() {
	}

	public static ProductService getInstance() {
		if (instance == null) {
			return new ProductService();
		}
		return instance;
	}

	public List<Product> getProducts() {
		return new ArrayList<Product>(productList.values());
	}

	public Product addMessage(Product product) {
		product.setId(productList.size() + 1);
		productList.put(product.getId(), product);
		return product;
	}

	public void removeProduct(Long id) {
		productList.remove(id);
	}
}
